import psycopg2
from sshtunnel import SSHTunnelForwarder
from faker import Faker
import random

# Constants
DB_HOST = 'dbpg.cs.ui.ac.id'
DB_USERNAME = 'db2001rp'
DB_PASSWORD = '17693'
DB_PORT = '5432'
DB_NAME = 'db2001rp'
USE_SSH = False
SSH_USERNAME = 'beno'
SSH_PASSWORD = 'rahasia'


def main():
    try:
        if USE_SSH:
            tunnel = SSHTunnelForwarder(('kawung.cs.ui.ac.id', 12122),
                                        ssh_username=SSH_USERNAME,
                                        ssh_password=SSH_PASSWORD,
                                        remote_bind_address=(DB_HOST, 5432),
                                        local_bind_address=('localhost', 5432))

            tunnel.start()

        conn = psycopg2.connect(user=DB_USERNAME,
                                password=DB_PASSWORD,
                                host=tunnel.local_bind_host,
                                port=tunnel.local_bind_port,
                                database=DB_NAME)

        cursor = conn.cursor()

        fake = Faker()

        # Insert PENGGUNA
        print('Generate PENGGUNA')
        pengguna = []
        for _ in range(100):
            query = 'INSERT INTO "silau"."pengguna" ("email", "password", "nama_lengkap", "alamat", "no_hp", "jenis_kelamin") VALUES (%s, %s, %s, %s, %s, %s)'
            args = (
                fake.email(), fake.password(), fake.name(), fake.address(), fake.phone_number()[0:20],
                random.choice(['M', 'F']))
            cursor.execute(query, args)
            pengguna.append(args[0])
            conn.commit()

        # Insert PELANGGAN
        pelanggan = []
        print('Generate PELANGGAN')
        for _ in range(70):
            query = 'INSERT INTO "silau"."pelanggan" ("email", "no_virtual_account", "saldo_dpay") VALUES (%s, %s, %s)'
            args = (random.choice(pengguna), fake.random_number(), fake.random_int())
            cursor.execute(query, args)
            pengguna.remove(args[0])
            pelanggan.append(args[0])
            conn.commit()

        # Insert STAF
        staff = []
        print('Generate STAF')
        for _ in range(10):
            query = 'INSERT INTO "silau"."staf" ("email", "npwp", "no_rekening", "nama_bank", "kantor_cabang") VALUES (%s, %s, %s, %s, %s)'
            args = (random.choice(pengguna), fake.ssn(), fake.credit_card_number(), random.choice(['BNI', 'BCA']),
                    random.choice(['DEPOK', 'JAKARTA']))
            cursor.execute(query, args)
            pengguna.remove(args[0])
            staff.append(args[0])
            conn.commit()

        # Insert KURIR
        kurir = []
        print('Generate KURIR')
        for _ in range(20):
            query = 'INSERT INTO "silau"."kurir" ("email", "npwp", "no_rekening", "nama_bank", "kantor_cabang", "no_sim", "nomor_kendaraan", "jenis_kendaraan") VALUES (%s, %s, %s, %s, %s, %s, %s, %s)'
            args = (random.choice(pengguna), fake.ssn(), fake.credit_card_number(), random.choice(['BNI', 'BCA']),
                    random.choice(['DEPOK', 'JAKARTA']), fake.ssn(), fake.ssn(), random.choice(['MOBIL', 'MOTOR']))
            cursor.execute(query, args)
            pengguna.remove(args[0])
            kurir.append(args[0])
            conn.commit()

        # Insert ITEM
        items = []
        print('Generate ITEM')
        for _ in range(10):
            query = 'INSERT INTO "silau"."item" ("kode", "nama") VALUES (%s, %s)'
            args = (fake.random_int(), fake.cryptocurrency_name())
            items.append(args[0])
            cursor.execute(query, args)
            conn.commit()

        # Insert LAYANAN
        layanan = []
        print('Generate LAYANAN')
        for _ in range(3):
            query = 'INSERT INTO "silau"."layanan" ("kode", "nama", "durasi", "harga") VALUES (%s, %s, %s, %s)'
            args = (fake.random_int(), fake.cryptocurrency_name(), fake.random_int(), fake.random_int())
            layanan.append(args[0])
            cursor.execute(query, args)
            conn.commit()

        # Insert STATUS
        status = []
        print('Generate STATUS')
        statuses = ['DROPPED', 'ON_PROGRESS', 'FINISHED', 'DELIVERED', 'REVIEWED']
        for i in range(5):
            query = 'INSERT INTO "silau"."status" ("kode", "nama") VALUES (%s, %s)'
            args = (fake.random_int(), statuses[i])
            status.append(args[0])
            cursor.execute(query, args)
            conn.commit()

        # Insert TARIF ANTAR JEMPUT
        tarif = []
        print('Generate TARIF ANTAR JEMPUT')
        for _ in range(2):
            query = 'INSERT INTO "silau"."tarif_antar_jemput" ("kode", "jarak_min", "jarak_max", "harga") VALUES (%s, %s, %s, %s)'
            args = (fake.random_int(), fake.random_int(), fake.random_int(), fake.random_int())
            tarif.append(args[0])
            cursor.execute(query, args)
            conn.commit()

        # Insert TRANSAKSI TOPUP DPAY
        topup = []
        print('Generate TRANSAKSI TOPUP DPAY')
        for _ in range(60):
            query = 'INSERT INTO "silau"."transaksi_topup_dpay" ("email", "tanggal", "nominal") VALUES (%s, %s, %s)'
            args = (random.choice(pelanggan), fake.date_of_birth(), fake.random_int())
            cursor.execute(query, args)
            conn.commit()

        # Insert TRANSAKSI LAUNDRY
        transactions = []
        print('Generate TRANSAKSI LAUNDRY')
        for _ in range(70):
            query = 'INSERT INTO "silau"."transaksi_laundry" ("email", "tanggal", "berat", "total_item", "biaya_laundry", "biaya_antar", "biaya_jemput", "diskon", "total_harga", "foto_pengambilan", "kode_layanan", "kode_tarif", "email_staf", "email_kurir_antar", "email_kurir_jemput") VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'
            args = (
                random.choice(pelanggan), fake.date_of_birth(), fake.random_int(), fake.random_int(), fake.random_int(),
                fake.random_int(), fake.random_int(), fake.random_int(), fake.random_int(), fake.image_url(),
                random.choice(layanan), random.choice(tarif), random.choice(staff), random.choice(kurir),
                random.choice(kurir))
            transactions.append(args)
            cursor.execute(query, args)
            conn.commit()

        # Insert DAFTAR LAUNDRY
        print('Generate DAFTAR LAUNDRY')
        for t in transactions:
            query = 'INSERT INTO "silau"."daftar_laundry" ("tanggal_transaksi", "no_urut", "jumlah_item", "kode_item", "email") VALUES (%s, %s, %s, %s, %s)'
            args = (t[1], fake.random_int(), fake.random_int(), random.choice(items), t[0])
            cursor.execute(query, args)
            conn.commit()

        # Insert TESTIMONI
        print('Generate TESTIMONI')
        for _ in range(50):
            query = 'INSERT INTO "silau"."testimoni" ("email", "tanggal_transaksi", "tanggal_testimoni", "status", "rating") VALUES (%s, %s, %s, %s, %s)'
            trx = random.choice(transactions)
            args = (trx[0], trx[1], fake.date_of_birth(), fake.text(), random.randrange(5))
            cursor.execute(query, args)
            conn.commit()

        # Insert STATUS TRANSAKSI
        print('Generate STATUS TRANSAKSI')
        for trx in transactions:
            query = 'INSERT INTO "silau"."status_transaksi" ("email", "tanggal_transaksi", "kode_status", "timestamp") VALUES (%s, %s, %s, %s)'
            for s in status:
                args = (trx[0], trx[1], s, fake.date_of_birth())
                cursor.execute(query, args)
                conn.commit()


    except (Exception, psycopg2.error) as error:
        print(error)


if __name__ == '__main__':
    main()
